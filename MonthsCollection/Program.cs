﻿using System;

namespace MonthsCollection
{
    class Program
    {
        private static int GetIntInput()
        {
            int input;
            Console.Write(">");
            try
            {
                input = Int32.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\nWrong input! Try again");
                return GetIntInput();
            }
            return input;
        }

        static void Main(string[] args)
        {
            var Months = new MonthCollection();
            Console.WriteLine("Enter months number: ");
            int i = GetIntInput();
            try
            {
                var m = Months.GetByNumber(i);
                Console.WriteLine(m.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("Enter months days: ");
            i = GetIntInput();
            try
            {
                var m = Months.GetMonthsByDays(i);
                foreach (var item in m)
                {
                    Console.WriteLine(item.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("Enter months name: ");
            string name = Console.ReadLine();
            try
            {
                var m = Months.GetByName(name);
                Console.WriteLine(m.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
