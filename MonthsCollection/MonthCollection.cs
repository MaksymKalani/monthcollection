﻿using System;
using System.Collections.Generic;

namespace MonthsCollection
{
    public class MonthCollection
    {
        private List<Month> _months { get; set; }
        public MonthCollection()
        {
            _months = new List<Month>();
            _months.Add(new Month("Jan", 31));
            _months.Add(new Month("Feb", 28));
            _months.Add(new Month("Mar", 31));
            _months.Add(new Month("Apr", 30));
            _months.Add(new Month("May", 31));
            _months.Add(new Month("Jun", 30));
            _months.Add(new Month("Jul", 31));
            _months.Add(new Month("Aug", 31));
            _months.Add(new Month("Sep", 30));
            _months.Add(new Month("Oct", 31));
            _months.Add(new Month("Nov", 30));
            _months.Add(new Month("Dec", 31));
        }
        public List<Month> GetMonthsByDays(int days)
        {
            List<Month> result = new List<Month>();
            foreach (var m in _months)
            {
                if (m.Days == days)
                {
                    result.Add(m);
                }
            }
            if (result.Count == 0)
                throw new ArgumentException($"There is no months with {days} days");
            return result;
        }
        public Month GetByNumber(int n)
        {
            try
            {
                return _months[n];
            }
            catch (Exception ex)
            {
                throw new ArgumentException($"{n} is not in range of 0-11");
            }
        }

        public Month GetByName(string n)
        {
            foreach (var m in _months)
            {
                if (m.Name == n)
                    return m;
            }
            throw new ArgumentException("Month not found. Input string should contain first three letters of month name");
        }
    }
}
