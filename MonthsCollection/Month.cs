﻿namespace MonthsCollection
{
    public class Month
    {
        public string Name { get; set; }
        public int Days { get; set; }
        public Month(string name, int days)
        {
            this.Name = name;
            this.Days = days;
        }

        public override string ToString()
        {
            return Name + " " + Days.ToString();
        }
    }
}
